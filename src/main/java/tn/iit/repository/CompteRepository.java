package tn.iit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.entity.Compte;

import java.util.List;

@Repository
public interface CompteRepository extends JpaRepository<Compte, String> {
    List<Compte> findByClient_Cin(Long cin);
}
