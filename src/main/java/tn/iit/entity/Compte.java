package tn.iit.entity;

import java.io.Serial;
import java.io.Serializable;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.EqualsAndHashCode.Include;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder
@Entity
@Table(name = "t_compte")
public class Compte implements Serializable{
	@Serial
	private static final long serialVersionUID = 1L;

	@Include
	@Id
	@Size(min = 22, max = 22)
	private String rib;
	@ManyToOne()
	@JoinColumn(name = "client_cin")
	private Client client;
	private float solde;



}
