package tn.iit.entity;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;
import lombok.EqualsAndHashCode.Include;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder

@Entity
@Table(name = "t_client")
public class Client implements Serializable {
	@Serial
	private static final long serialVersionUID = 1L;
	@Include
	@Id
	@Column(name = "numero_carte_identite")
	@Size(min = 8, max = 8)
	@Digits(integer = 8, fraction = 0)
	private Long cin;
	@Column(name = "nom_client")
	private String nom;
	@Column(name = "prenom_client")
	private String prenom;

}
