package tn.iit.controller;

import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;
import tn.iit.core.ServerResponse;
import tn.iit.dto.request.SignUpRequest;
import tn.iit.dto.request.SigninRequest;
import tn.iit.dto.response.JwtAuthenticationResponse;
import tn.iit.service.AuthenticationService;

@RestController
@RequestMapping("/auth")
@CrossOrigin("*")
@RequiredArgsConstructor
public class AuthenticationController  {
    private final AuthenticationService authenticationService;
    @PostMapping("/signup")
    public ServerResponse<JwtAuthenticationResponse> signup(@RequestBody SignUpRequest request) {
        return authenticationService.signup(request);
    }

    @PostMapping("/signin")
    public ServerResponse<JwtAuthenticationResponse> signin(@RequestBody SigninRequest request) {
        return authenticationService.signin(request);
    }
}
