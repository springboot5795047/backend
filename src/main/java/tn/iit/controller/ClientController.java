package tn.iit.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import tn.iit.core.ServerResponse;
import tn.iit.dto.request.ClientRequest;
import tn.iit.dto.response.ClientResponse;
import tn.iit.entity.Client;
import tn.iit.service.ClientService;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/client")
@AllArgsConstructor
public class ClientController {

    private ClientService mClientService;
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des clients",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))}),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Récupérer toutes les clients ",
            description = "Tous les clients sont renvoyés"
    )
    @GetMapping()
    public ServerResponse<List<ClientResponse>>  getAll() {
        return mClientService.getAll();
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Client trouvé",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Client non trouvé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Récupérer le Client par identifiant",
            description = "Le Client est renvoyé"
    )
    @GetMapping(value = "/{cin}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServerResponse<ClientResponse> getClient(@PathVariable Long cin) {
        return mClientService.getByCin(cin);
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Client créé",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))}),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "400", description = "Client existe déja",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Créer un nouveau Client ",
            description = "Un Client a été créé"
    )
    @PostMapping()
    public ServerResponse<ClientResponse> createClient(@Valid @RequestBody ClientRequest clientRequest) {
        return mClientService.create(clientRequest);
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Client mise à jour",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Client non trouvé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Mettre à jour le Client",
            description = "Un Client a été mise à jour"
    )
    @PutMapping("/{cin}")
    public ServerResponse<ClientResponse> updateClient(@RequestBody Client client, @PathVariable Long cin) {
        return mClientService.update(client, cin);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Client supprimé",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Client non trouvé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Supprimer le Client",
            description = "Un Client a été supprimé"
    )
    @DeleteMapping("/{cin}")
    public ServerResponse<ClientResponse> deleteClient(@PathVariable Long cin) {
        return mClientService.delete(cin);
    }
}
