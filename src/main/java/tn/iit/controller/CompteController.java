package tn.iit.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;

import org.springframework.web.bind.annotation.*;
import tn.iit.core.ServerResponse;
import tn.iit.dto.request.CompteRequest;
import tn.iit.dto.response.CompteResponse;
import tn.iit.entity.Compte;
import tn.iit.service.CompteService;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/compte")
@AllArgsConstructor
public class CompteController {
    private CompteService mCompteService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des comptes",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CompteResponse.class))}),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Récupérer toutes les comptes ",
            description = "Tous les comptes sont renvoyés"
    )
    @GetMapping()
    public ServerResponse<List<CompteResponse>> getAll() {
        return mCompteService.getAll();
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des comptes d'un client",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CompteResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Client n'a pas de compte",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Client non trouvé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Récupérer toutes les comptes d'un client",
            description = "Tous les comptes d'un client sont renvoyés")
    @GetMapping(value = "/getAllByClient/{cin}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServerResponse<List<CompteResponse>> getAllByClient(@PathVariable Long cin) {
        return mCompteService.getAllByClient(cin);
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Compte trouvé",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CompteResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Compte non trouvé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Récupérer le compte par identifiant",
            description = "Le compte est renvoyé"
    )
    @GetMapping(value = "/{rib}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServerResponse<CompteResponse> getCompte(@PathVariable String rib) {
        return mCompteService.getByRib(rib);
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Comptes du client trouvés",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CompteResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Comptes non trouvé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Récupérer les compte par client",
            description = "Le comptes sont renvoyés"
    )
    @GetMapping(value = "/compteByClient/{cin}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServerResponse<List<CompteResponse>> getCompteByClient(@PathVariable Long cin) {
        return mCompteService.getAllByClient(cin);
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Compte créé",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CompteResponse.class))}),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "400", description = "Compte existe déja",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Créer un nouveau compte ",
            description = "Un compte a été créé"
    )
    @PostMapping()
    public ServerResponse<CompteResponse> createCompte(@Valid @RequestBody CompteRequest compteRequest) {
        return mCompteService.create(compteRequest);
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Compte mise à jour",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CompteResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Compte non trouvé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Mettre à jour le compte",
            description = "Un compte a été mise à jour"
    )
    @PutMapping("/{rib}")
    public ServerResponse<CompteResponse> updateCompte(@RequestBody Compte compte, @PathVariable String rib) {
        return mCompteService.update(compte, rib);
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Compte supprimé",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CompteResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Identifiant invalide fourni",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Compte non trouvé",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "405", description = "Methode n'est pas autorisé",
                    content = @Content(mediaType = "application/json")),
    })
    @Operation(summary = "Supprimer le compte",
            description = "Un compte a été supprimé"
    )
    @DeleteMapping("/{rib}")
    public ServerResponse<CompteResponse> deleteCompte(@PathVariable String rib) {
        System.out.println(rib);
        return mCompteService.delete(rib);
    }
}
