package tn.iit.service;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import tn.iit.core.ServerResponse;
import tn.iit.dto.mapper.CompteMapper;
import tn.iit.dto.request.CompteRequest;
import tn.iit.dto.response.ClientResponse;
import tn.iit.dto.response.CompteResponse;
import tn.iit.entity.Compte;
import tn.iit.repository.CompteRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class CompteService {

    private CompteRepository mCompteRepository;
    private CompteMapper mCompteMapper;
    private ClientService mClientService;

    public ServerResponse<List<CompteResponse>> getAll() {
        List<CompteResponse> compteResponses = mCompteRepository.findAll()
                .stream()
                .map(mCompteMapper::toDTO)
                .collect(Collectors.toList());
        ServerResponse<List<CompteResponse>> apiResponse = new ServerResponse<>();
        return apiResponse.success(HttpStatus.OK.value(), compteResponses);
    }
    public ServerResponse<List<CompteResponse>> getAllByClient(Long clientCin) {
        List<CompteResponse> compteResponses = mCompteRepository.findByClient_Cin(clientCin)
                .stream()
                .map(mCompteMapper::toDTO)
                .collect(Collectors.toList());
        ServerResponse<List<CompteResponse>> apiResponse = new ServerResponse<>();
        if(compteResponses.isEmpty()){
            ServerResponse<ClientResponse> client = mClientService.getByCin(clientCin);
            if(client.getData()==null){
                return apiResponse.echec(HttpStatus.NOT_FOUND.value(),"Client n'existe pas");
            }else{
                return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"Client n'a pas de compte");
            }
        }
        else{
            return apiResponse.success(HttpStatus.OK.value(), compteResponses);
        }

    }
    public ServerResponse<CompteResponse> getByRib(String ribCompte) {
        Optional<Compte> compte= mCompteRepository.findById(ribCompte);
        ServerResponse<CompteResponse> apiResponse = new ServerResponse<>();
        if (compte.isPresent()){
            return apiResponse.success(HttpStatus.OK.value(), mCompteMapper.toDTO(compte.get()));
        }
        else{
            return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"IDENTIFIANT INVALIDE");
        }
    }

    public ServerResponse<CompteResponse> create(CompteRequest compteRequest) {
        Compte mCompte = mCompteMapper.fromDTO(compteRequest);
        Optional <Compte> compte= mCompteRepository.findById(mCompte.getRib());
        ServerResponse<CompteResponse> apiResponse = new ServerResponse<>();
        if (compte.isPresent()){
            return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"exist");
        }
        else{
            ServerResponse<ClientResponse> client = mClientService.getByCin(compteRequest.getClient().getCin());
            if(client.getData()==null){
                return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"Client n'existe pas");
            }
            else{
                Compte compteSaved = mCompteRepository.save(mCompte);
                return apiResponse.success(HttpStatus.CREATED.value(), mCompteMapper.toDTO(compteSaved));
            }

        }
    }
    public ServerResponse<CompteResponse> update(Compte compte, String rib) {
        Optional <Compte> compteToUpdate = mCompteRepository.findById(rib);
        ServerResponse<CompteResponse> apiResponse = new ServerResponse<>();
        if (compteToUpdate.isPresent()){
            Compte getCompteToUpdate = compteToUpdate.get();
            getCompteToUpdate.setSolde(compte.getSolde());
            Compte compteUpdated = mCompteRepository.save(getCompteToUpdate);
            return apiResponse.success(HttpStatus.OK.value(), mCompteMapper.toDTO(compteUpdated));
        }
        else{
            return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"IDENTIFIANT INVALIDE");
        }
    }
    public ServerResponse<CompteResponse> delete(String rib) {
        Optional <Compte> compteToDelete = mCompteRepository.findById(rib);
        System.out.println(rib);
        ServerResponse<CompteResponse> apiResponse = new ServerResponse<>();
        if (compteToDelete.isPresent()){
            mCompteRepository.deleteById(rib);
            return apiResponse.success(HttpStatus.OK.value(), null);
        }
        else{
            return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"IDENTIFIANT INVALIDE");
        }
    }

}
