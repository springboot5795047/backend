package tn.iit.service;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import tn.iit.core.ServerResponse;
import tn.iit.dto.mapper.ClientMapper;
import tn.iit.dto.request.ClientRequest;
import tn.iit.dto.response.ClientResponse;
import tn.iit.entity.Client;
import tn.iit.entity.Compte;
import tn.iit.repository.ClientRepository;
import tn.iit.repository.CompteRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class ClientService {

    private ClientRepository mClientRepository;
    private ClientMapper mClientMapper;
    private CompteRepository mCompteRepository;

    public ServerResponse<List<ClientResponse>>  getAll() {
       List<ClientResponse> clientResponses = mClientRepository.findAll()
               .stream()
               .map(mClientMapper::toDTO)
               .collect(Collectors.toList());
       ServerResponse<List<ClientResponse>> apiResponse = new ServerResponse<>();
       return apiResponse.success(HttpStatus.OK.value(), clientResponses);
    }
    public ServerResponse<ClientResponse> getByCin(Long clientCin) {
         Optional <Client> client= mClientRepository.findById(clientCin);
         ServerResponse<ClientResponse> apiResponse = new ServerResponse<>();
         if (client.isPresent()){
             return apiResponse.success(HttpStatus.OK.value(), mClientMapper.toDTO(client.get()));
         }
         else{
             return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"IDENTIFIANT INVALIDE");
         }
    }

    public ServerResponse<ClientResponse> create(ClientRequest clientRequest) {
        Client mClient = mClientMapper.fromDTO(clientRequest);
        Optional <Client> client= mClientRepository.findById(mClient.getCin());
        ServerResponse<ClientResponse> apiResponse = new ServerResponse<>();
        if (client.isPresent()){
            return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"exist");
        }
        else{
            Client clientSaved = mClientRepository.save(mClient);
            return apiResponse.success(HttpStatus.CREATED.value(), mClientMapper.toDTO(clientSaved));
        }
    }

    public ServerResponse<ClientResponse> update(Client client, Long cin) {
        Optional <Client> clientToUpdate = mClientRepository.findById(cin);
        ServerResponse<ClientResponse> apiResponse = new ServerResponse<>();
        if (clientToUpdate.isPresent()){
            Client getClientToUpdate = clientToUpdate.get();
            getClientToUpdate.setPrenom(client.getPrenom());
            getClientToUpdate.setNom(client.getNom());
            Client clientUpdated = mClientRepository.save(getClientToUpdate);
            return apiResponse.success(HttpStatus.OK.value(), mClientMapper.toDTO(clientUpdated));
        }
        else{
            return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"IDENTIFIANT INVALIDE");
        }
    }

    public ServerResponse<ClientResponse> delete(Long cin) {
        Optional <Client> clientToDelete = mClientRepository.findById(cin);
        ServerResponse<ClientResponse> apiResponse = new ServerResponse<>();
        if (clientToDelete.isPresent()){
            for (Compte compte : mCompteRepository.findByClient_Cin(cin)) {
                mCompteRepository.deleteById(compte.getRib());
            }

            mClientRepository.deleteById(cin);

            return apiResponse.success(HttpStatus.OK.value(), null);
        }
        else{
            return apiResponse.echec(HttpStatus.BAD_REQUEST.value(),"IDENTIFIANT INVALIDE");
        }
    }
}
