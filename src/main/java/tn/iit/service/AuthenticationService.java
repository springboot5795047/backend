package tn.iit.service;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import tn.iit.core.ServerResponse;
import tn.iit.dto.response.JwtAuthenticationResponse;
import tn.iit.dto.request.SignUpRequest;
import tn.iit.dto.request.SigninRequest;
import tn.iit.entity.Role;
import tn.iit.entity.User;
import tn.iit.repository.UserRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public ServerResponse<JwtAuthenticationResponse> signup(SignUpRequest request) {
        User user = User.builder().firstName(request.getFirstName()).lastName(request.getLastName())
                .email(request.getEmail()).password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER).build();
        userRepository.save(user);
        var jwt = jwtService.generateToken(user);
        ServerResponse<JwtAuthenticationResponse> apiResponse = new ServerResponse<>();
        return apiResponse.success(HttpStatus.OK.value(), JwtAuthenticationResponse.builder().token(jwt).build());
    }

    public ServerResponse<JwtAuthenticationResponse>  signin(SigninRequest request) {
        ServerResponse<JwtAuthenticationResponse> apiResponse = new ServerResponse<>();
        Optional<User> existingUser = userRepository.findByEmail(request.getEmail());
        if (existingUser.isPresent()) {

            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
            User user = existingUser.get();
            var jwt = jwtService.generateToken(user);

            return apiResponse.success(HttpStatus.OK.value(), JwtAuthenticationResponse.builder().token(jwt).build());
        } else {
            return apiResponse.echec(HttpStatus.BAD_REQUEST.value(), "Invalid email or password");
        }
    }

}
