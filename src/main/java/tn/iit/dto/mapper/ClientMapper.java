package tn.iit.dto.mapper;

import org.springframework.stereotype.Component;
import tn.iit.dto.request.ClientRequest;
import tn.iit.dto.response.ClientResponse;
import tn.iit.entity.Client;

@Component
public class ClientMapper {
    public Client fromDTO(ClientRequest clientDTO) {
        return Client.builder()
                .cin(clientDTO.getCin())
                .nom(clientDTO.getNom())
                .prenom(clientDTO.getPrenom())
                .build();
    }

    public ClientResponse toDTO(Client client) {
        return ClientResponse
                .builder()
                .cin(client.getCin())
                .nom(client.getNom())
                .prenom(client.getPrenom())
                .build();
    }
}
