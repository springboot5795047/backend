package tn.iit.dto.mapper;

import org.springframework.stereotype.Component;

import tn.iit.dto.request.CompteRequest;

import tn.iit.dto.response.CompteResponse;
import tn.iit.entity.Compte;

@Component
public class CompteMapper
{
    public Compte fromDTO(CompteRequest compteDTO) {
        return Compte.builder()
                .rib(compteDTO.getRib())
                .solde(compteDTO.getSolde())
                .client(compteDTO.getClient())
                .build();
    }

    public CompteResponse toDTO(Compte compte) {
        return CompteResponse
                .builder()
                .rib(compte.getRib())
                .solde(compte.getSolde())
                .client(compte.getClient())
                .build();
    }
}
