package tn.iit.dto.response;

import lombok.*;
import tn.iit.entity.Client;

@Data
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Builder
public class CompteResponse {
    private String rib;
    private Client client;
    private float solde;
}
