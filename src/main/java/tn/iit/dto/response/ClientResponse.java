package tn.iit.dto.response;

import lombok.*;

@Data
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Builder
public class ClientResponse {
    private Long cin;
    private String nom;
    private String prenom;

}
