package tn.iit.dto.request;

import lombok.*;
import tn.iit.entity.Client;

@Data
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class CompteRequest {
    private String rib;
    private Client client;
    private float solde;
}
