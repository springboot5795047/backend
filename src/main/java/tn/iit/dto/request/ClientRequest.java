
package tn.iit.dto.request;
import lombok.*;
@Data
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ClientRequest {
    private Long cin;
    private String nom;
    private String prenom;
}